taille = 0.0
poids = 0.0
imc = 0.0
sexe = ""
age = 0

taille = float(input("Entrer votre taille en mètre: "))
poids = float(input("Entrer votre poids en kilogramme: "))
sexe = input("Entrer votre sexe (M/F): ")
age = int(input("Entrer votre age: "))

imc = poids / (taille * taille)

print("Votre IMC est de: ", imc)

if sexe == "M":
    if age >= 20 and age <= 39:
        if imc >= 25:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    elif age >= 40 and age <= 59:
        if imc >= 26:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    elif age >= 60:
        if imc >= 27:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    else:
        print("Vous n'êtes pas dans la tranche d'âge")
elif sexe == "F":
    if age >= 20 and age <= 39:
        if imc >= 23:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    elif age >= 40 and age <= 59:
        if imc >= 24:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    elif age >= 60:
        if imc >= 25:
            print("Vous êtes en surpoids")
        else:
            print("Vous n'êtes pas en surpoids")
    else:
        print("Vous n'êtes pas dans la tranche d'âge")
else:
    print("Vous n'avez pas entré un sexe valide")
